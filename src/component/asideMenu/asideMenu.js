import React, { Component } from 'react';
import Home from './img/home.png';
import Chat from './img/chat.png';
import Groups from './img/group.png';
import Training from './img/training.png';
import Events from './img/event.png';
import Documents from './img/document.png';
import People from './img/people.png';
import Email from './img/email.png';
import Extra from './img/extra.png';
import "./css/asideMenu.scss";
import AsideBtn from '../asideBtn/asideBtn';

export default class AsideMenu extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            images: [
                {
                    title: 'Home',
                    src: Home
                },
                {
                    title: 'Chat',
                    src: Chat
                },
                {
                    title: 'Groups', 
                    src: Groups 
                },
                {
                    title: 'Training',
                    src: Training
                },
                {
                    title: 'Events',
                    src: Events
                },
                {
                    title: 'Documents',
                    src: Documents
                },
                {
                    title: 'People',
                    src: People
                },
                {
                    title: 'Email',
                    src: Email
                },
                {
                    title: 'Extra',
                    src: Extra
                }
            ]
        }
    }
    
    render() {
        return (
            <div className={this.props.classes}>
                <img src={require('./img/logo.png')} alt="logo" style={{marginBottom: 50}}/>

                {
                    this.state.images.map((element) => {
                        return (
                            <AsideBtn src={element.src} alt={element.title}/>
                        )
                    })
                }

            </div>
        )
    }
}
