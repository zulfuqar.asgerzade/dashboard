import React, { Component } from 'react';
import MontlyGraph from '../montlyGraph/montlyGraph';
import MonthlyPie from '../monthlyPie/monthlyPie'
import './css/montlyData.scss';

export default class MonthlyDataContainer extends Component {
    render() {
        return (
            <div className="montlyDataContainer">
                <MontlyGraph />
                <MonthlyPie />
            </div>
        )
    }
}
