import React, { Component } from 'react';
import './css/asideBtn.scss';

export default class AsideBtn extends Component {
    render() {
        return (
            <div className="asideBtn">
                <div className="dotsContainer">
                    <div className="dotsContainer_dot"></div>
                    <div className="dotsContainer_dot"></div>
                    <div className="dotsContainer_dot"></div>
                </div>
                <img src={this.props.src} alt={this.props.alt}/>
                <p className="asideBtn_title">{this.props.alt}</p>
            </div>
        )
    }
}
