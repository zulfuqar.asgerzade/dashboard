import React, { Component } from 'react';
import './css/navbar.scss';

export default class Navbar extends Component {

    render() {
        return (
            <nav className="navbar">
                <div className="navbar_menuContainer">
                    <button onClick={this.props.isOpenHandler} className="navbar_menuBtn">

                    </button>
                    <div className="navbar_searchContainer">
                        <img src={require('./img/searchIcon.png')} alt="search"/>
                        <input type="text" className="navbar_search" placeholder="Search..."/>
                    </div>
                </div>
                <div className="notificationContainer">
                    <div className="dropDownContainer">
                        <img src={require('./img/plus.png')} alt="plusIcon"/>
                        <select name="" id="" className="dropDownContainer_dropDown">
                            <option value=""  defaultChecked>Create</option>
                        </select>
                    </div>
                    <div className="notificationContainer_iconContainer">
                        <div className="notification">3</div>
                        <img src={require('./img/tasks.png')} alt="itaskIcon"/>
                    </div>
                    <div className="notificationContainer_iconContainer">
                        <div className="notification">3</div>
                        <img src={require('./img/notification.png')} alt="alertIcon"/>
                    </div>
                    <img src={require('./img/profilePhoto.png')} alt="" className="notificationContainer_profilePhoto"/>
                </div>
            </nav>
        )
    }
}
