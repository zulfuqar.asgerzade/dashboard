import React, { Component } from 'react';
import CircleBar from '../circleBar/circleBar'
import './css/circleBarContainer.scss'

export default class CircleBarContainer extends Component {

    render() {
        return (
            <div className="progressBarContainer">
            <div className="progressBar_box">
                <CircleBar value="90" color="#3498db"/>
                <p className="progressBar_title">Overall Progress</p>
            </div>
            <div className="progressBar_box">
                <CircleBar value="63" color="#dc677d"/>
                <p className="progressBar_title">New Admissions</p>
            </div>
            <div className="progressBar_box">
                <CircleBar value="32" color="#ae6ec3"/>
                <p className="progressBar_title">All Graduates</p>
            </div>
            <div className="progressBar_box">
                <CircleBar value="52" color="#e74c3c"/>
                <p className="progressBar_title">Shcolarships</p>  
            </div>
        </div>

        )
    }
}
