import React, { Component } from 'react';
import CircleBarContainer from '../circleBarContainer/circleBarContainer';
import MonthlyDataContainer from '../montlyDataContainer/monthlyDataContainer';
import MainPage from '../mainPage/mainPage'
import './css/overview.scss';

export default class Overview extends Component {
    render() {
        return (
            <div className="overviewContainer">
                <MainPage/>
                <CircleBarContainer/>
                <MonthlyDataContainer/>
            </div>
        )
    }
}
