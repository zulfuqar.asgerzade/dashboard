import React, { useState } from 'react';
import AsideMenu from './component/asideMenu/asideMenu';
import Navbar from './component/navbar/navbar';
import Overview from './component/overview/overview'
import './App.css';


function App() {

  const[isOpen, setIsOpen] = useState(true);

  const setActive = () => {
    setIsOpen(!isOpen);
  }

  let classes = "asideMenu";

  if (isOpen) {
    classes += " active"
  }

  let asideMenu = isOpen ? <AsideMenu classes={classes}/> : null ;

  return (
    <div className="mainContainer">
      {asideMenu}
     <div className="centerContainer">
        <Navbar  isOpenHandler = {setActive}/> 
        <Overview/>
      </div>
    </div>
  );
}
 
export default App;
